---
lang: en
---

# Fingertips Force

Empowers the way your fingertips control computers.

A look at the very first draft : ![3d model of fingertips force](draft1.png)

The firmware lives on another
[GitLab repository](https://gitlab.com/fingertipsforce/case).

Copyright (C) 2025 Guilhem Fauré, email me at pro@gfaure.eu

## TODO

- Consider using parametric and programmatic modelisation like OpenSCAD.

## TODO for v0.2.2

> This version shall not be printed if the currently printed on is sufficient to
> properly develop the firmware and test the concept

### Lid

- Make internal edge slightly less large to compensate printer’s imprecisions

- Double check sticks dimmensions
- Make sticks enclosure a bit larger to compensate for printer’s imprecisions
- Make lid thickness above sticks tighter to prevent the case from limiting
  movement
- Correct the sticks holes placement

- Move arc pillars slightly towards the center (far from the arc border)
- Move arc pillars slightly away frow each other
- Move back pillar slightly towards the center
- Remove center pillar and rectangle fill between it and back pillar
- Make all pillars square and slightly thicker
- Add a pillar at center, close to the arc

- Round more angles between arc and vertical lines

### Case

- Move pillars screw holes at their new positions
- Add screw hole for new pillar

- Round more angles between arc and vertical lines

## TODO for v0.3.0

> This version may not be actually built Don’t forget v0.2.2 improvements

Totally new model based on fingers length relative to pinky, and inclusion of
thumb sticks in one object.

### Lid

- Shape the lid and place sticks based on the relative fingers lengths
  - Sticks have a fixed horizontal spacing of about 26 mm
- Do not make an internal edge at the USB hole
- Add FPCs supports (double check dimmensions) with screw holes
- Add Arduino Micro supports with screw holes in the central empty zone (at the
  same height as FPCs)
- Add pillars with screw holes

### Case

- Follow the same shape as lid
- Do not use thickness, make the wall with a pad
  - The USB hole consists of 1 cm left without wall
- Add properly placed screw holes
- High adherence

### Thumb module

Add the thumb stick, ergonomically tilted, serving as a tilting support.
Eventually make a mecanism to adjust the tilt angle. Eventually make the tilting
a separate mecanism.

NEEDS FURTHER STUDY

#### Thumb module lid

- Thumb stick

#### Thumb module case

- Tilt support

## TODO for v0.4.0

> Do not forget previous evolutions

### Custom PCB

- Computer USB-C port
  - Output as an HMI device (keyboard, mouse and gamepad)
  - Receive arbritrary inputs (change of firmware, dictionnary…)
  - Receive electrecity (necessary energy)

- 5 × 5-pin FPC connectors
  - 5 × 2 analog inputs for sticks X and Y axis
  - 5 digital inputs for sticks middle clicks
- 1 or 2 additional digital input/output for mode switch led-button

- Necessary for a high-performance mouse (NEEDS FURTHER STUDY)

- Microcontroller and other electronics needed to handle this (NEEDS FURTHER
  STUDY)

### Lid

- Remove FPCs supports
- Add custom PCB’s supports with screw holes
