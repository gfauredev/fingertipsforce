{
  description = "Python env";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShells = {
          default = pkgs.mkShell {
            nativeBuildInputs = with pkgs; [
              uv # Python project and packages management
              ruff # Lint, Format, LSP
              mypy # Type check, launch as server with `dmypy start`
              # Libraries
              libGL
              libGLU
              mesa
              libsForQt5.qt5.qtbase
              libsForQt5.qmake
            ] ++ (with pkgs.python3Packages; [
              rope # Smart refactoring
              python-lsp-server # Python LSP
              pylsp-mypy # Mypy LSP
              pylsp-rope # Rope LSP
            ]);
          };
        };
      });
}
